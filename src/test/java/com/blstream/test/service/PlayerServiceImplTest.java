package com.blstream.test.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.blstream.test.model.Player;


@RunWith(SpringJUnit4ClassRunner.class) 
@ContextConfiguration(locations = {"classpath:/spring/spring-context.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@Transactional
public class PlayerServiceImplTest {

	@Autowired
	PlayerService playerService;
	
	@Test
	public void testAddPlayer_1() {
		Player player = new Player();
		player.setName("Player3");
		
		Player addedPlayer = playerService.addPlayer(player);	
		List<Player> players = playerService.getAllPlayers();
		
		assertNotNull(addedPlayer.getId());
		assertNotNull(players);
		assertEquals(3, players.size());
	}
	
	@Test
	public void testAddPlayer_2() {
		Player player = new Player();
		player.setId(1L);
		player.setName("Player3");
		
		playerService.addPlayer(player);
		
		List<Player> players = playerService.getAllPlayers();
		assertEquals(3, players.size());
	}
	
	@Test
	public void testDeletePlayer_1() {
		playerService.deletePlayer(0L);
		Player player = playerService.getPlayer(0L);
		List<Player> players = playerService.getAllPlayers();
		
		assertNull(player);
		assertEquals(1, players.size());
	}
	
	@Test
	public void testDeletePlayer_2() {
		playerService.deletePlayer(2L);
		List<Player> players = playerService.getAllPlayers();
		
		assertEquals(2, players.size());
	}
	
	@Test
	public void testGetAllPlayers_1() {
		List<Player> players = playerService.getAllPlayers();
		
		assertNotNull(players);
		assertEquals(2, players.size());
	}
	
	@Test
	public void testGetAllPlayers_2() {
		playerService.deletePlayer(0L);
		playerService.deletePlayer(1L);
		List<Player> players = playerService.getAllPlayers();
		
		assertNotNull(players);
		assertEquals(0, players.size());
	}
	
	@Test
	public void testGetPlayer() {
		Player player = playerService.getPlayer(0L);
		
		assertNotNull(player);
		assertEquals("Player1", player.getName());
	}
	
	@Test
	public void testUpdatePlayer_1() {
		Player player = new Player();
		player.setId(0L);
		player.setName("Player3");
		
		Player updated = playerService.updatePlayer(player);
		List<Player> players = playerService.getAllPlayers();
		
		assertNotNull(updated);		
		assertEquals(player.getName(), updated.getName());
		assertEquals(2, players.size());
	}
	
	@Test
	public void testUpdatePlayer_2() {
		Player player = new Player();
		player.setId(3L);
		player.setName("Player3");

		Player updated = playerService.updatePlayer(player);
		List<Player> players = playerService.getAllPlayers();
		
		assertNull(updated);
		assertEquals(2, players.size());
	}
}
