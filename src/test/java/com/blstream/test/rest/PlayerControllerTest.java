package com.blstream.test.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.net.URI;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.junit.Test;

import com.blstream.test.model.Player;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.ClientResponse.Status;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

/**
 * Not repeatable test bundle. Run at the start of a application.
 * For development purpose only.
 *
 */
public class PlayerControllerTest {
	
	private URI baseUri = UriBuilder.fromUri("http://localhost:8080/test/players").build();
	
	private Client getClient() {
		ClientConfig cfg = new DefaultClientConfig();
		cfg.getClasses().add(JacksonJsonProvider.class);
		return Client.create(cfg);
	}
	
	public void testAdd_ok() {
		Player player = new Player();
		player.setId(0L);
		player.setName("Player3");
			
		ClientResponse response = getClient()
				.resource(baseUri)
				.accept(MediaType.APPLICATION_JSON)
				.entity(player, MediaType.APPLICATION_JSON)
				.post(ClientResponse.class);
		Player playerReturned = response.getEntity(Player.class);
		
		assertEquals(201, response.getStatus());
		assertEquals(player.getName(), playerReturned.getName());
		assertNotEquals(null, playerReturned.getId());
	}
	
	public void testAdd_validationError() {
		Player player = new Player();
		player.setId(0L);
		player.setName("Play");
				
		GenericType<Map<String, Object>> generic = new GenericType<Map<String, Object>>(){};
		ClientResponse response = getClient()
				.resource(baseUri)
				.accept(MediaType.APPLICATION_JSON)
				.entity(player, MediaType.APPLICATION_JSON)
				.post(ClientResponse.class);
		Map<String, Object> errors = response.getEntity(generic);
		assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
		assertEquals(true, errors.containsKey("validation_errors"));
		
	}
	
	@Test
	public void testBundle() {
		// {0: Player1, 1:Player2}
		testGet_ok();
		testGet_notFound();
		testGetAll(2);
		// {0: Player1, 1:Player2}
		testDelete(3L);
		// {0: Player1, 1:Player2}
		testGetAll(2);	
		testDelete(0L);
		// {1:Player2}
		testGetAll(1);
		testAdd_ok();
		// {1:Player2, ?:Player3}
		testGetAll(2);
		testAdd_validationError();
		testGetAll(2);
		testUpdate_validationError(1L);
		testGetAll(2);
		testUpdate_notFound(4L);
		testUpdate_ok(1L);
		testGetAll(2);
	}
	
	public void testDelete(Long id) {
		ClientResponse response = getClient()
				.resource(baseUri).path(""+id)
				.delete(ClientResponse.class);
		
		assertEquals(200, response.getStatus());
	}
	
	public void testGet_notFound() {
		ClientResponse response = getClient()
				.resource(baseUri).path("2")
				.accept(MediaType.APPLICATION_JSON)
				.get(ClientResponse.class);
			
		assertEquals(404, response.getStatus());
	}
	
	public void testGet_ok() {
		ClientResponse response = getClient()
				.resource(baseUri).path("0")
				.accept(MediaType.APPLICATION_JSON)
				.get(ClientResponse.class);
		Player player = response.getEntity(Player.class);
		
		assertEquals(200, response.getStatus());
		assertEquals("Player1", player.getName());
	}
	
	public void testGetAll(int assertRemains) {
		GenericType<List<Player>> generic = new GenericType<List<Player>>(){};
		ClientResponse response = getClient()
				.resource(baseUri)
				.accept(MediaType.APPLICATION_JSON)
				.get(ClientResponse.class);
		List<Player> players = response.getEntity(generic);
		
		assertEquals(200, response.getStatus());
		assertEquals(assertRemains, players.size());
	}
	
	public void testUpdate_notFound(Long id) {
		Player player = new Player();
		player.setId(id);
		player.setName("Player4");
				
		ClientResponse response = getClient()
				.resource(baseUri)
				.accept(MediaType.APPLICATION_JSON)
				.entity(player, MediaType.APPLICATION_JSON)
				.put(ClientResponse.class);

		assertEquals(Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}
	
	public void testUpdate_ok(Long id) {
		Player player = new Player();
		player.setId(id);
		player.setName("Player4");
				
		ClientResponse response = getClient()
				.resource(baseUri)
				.accept(MediaType.APPLICATION_JSON)
				.entity(player, MediaType.APPLICATION_JSON)
				.put(ClientResponse.class);

		assertEquals(Status.OK.getStatusCode(), response.getStatus());
	}
	
	public void testUpdate_validationError(Long id) {
		Player player = new Player();
		player.setId(id);
		player.setName("Play");
				
		GenericType<Map<String, Object>> generic = new GenericType<Map<String, Object>>(){};
		ClientResponse response = getClient()
				.resource(baseUri)
				.accept(MediaType.APPLICATION_JSON)
				.entity(player, MediaType.APPLICATION_JSON)
				.put(ClientResponse.class);
		Map<String, Object> errors = response.getEntity(generic);
		assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
		assertEquals(true, errors.containsKey("validation_errors"));
	}
	
	
}
