package com.blstream.test.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import com.blstream.test.model.validation.Update;

@SuppressWarnings("serial")
@Entity
@Table(name = "players")
@XmlRootElement(name = "player")
public class Player implements Serializable {
	
	private Long id;
	private String name;
	private int version;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Player other = (Player) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (version != other.version)
			return false;
		return true;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "player_id")
	@Min(value = 0, groups = {Update.class})
	public Long getId() {
		return id;
	}

	@Column(name = "name")
	@Size(min = 5, max = 50)
	@Pattern(regexp = "[a-zA-Z]\\w+")
	public String getName() {
		return name;
	}

	@Version 
    @Column(name = "version") 
    public int getVersion() { 
        return this.version; 
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + version;
		return result;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return "Player [id=" + id + ", name=" + name + ", version=" + version
				+ "]";
	}
}
