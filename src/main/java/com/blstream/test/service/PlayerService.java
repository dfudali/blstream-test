package com.blstream.test.service;

import java.util.List;

import com.blstream.test.model.Player;

public interface PlayerService {
	
	public Player addPlayer(Player player);
	
	public void deletePlayer(Long id);
	
	public List<Player> getAllPlayers();
	
	public Player getPlayer(Long id);
	
	public Player updatePlayer(Player player);
}
