package com.blstream.test.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.blstream.test.dao.PlayerDAO;
import com.blstream.test.model.Player;

@Service
@Transactional
public class PlayerServiceImpl implements PlayerService {

	private PlayerDAO playerDao;

	@Override
	public Player addPlayer(Player player) {
		return playerDao.add(player);
	}
	
	@Override
	public void deletePlayer(Long id) {
		playerDao.deleteById(id);
	}	

	@Override
	@Transactional(readOnly = true)
	public List<Player> getAllPlayers() {
		return playerDao.getAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Player getPlayer(Long id) {
		return playerDao.get(id);
	}

	@Autowired
	public void setPlayerDAO(PlayerDAO playerDao) {
		this.playerDao = playerDao;
	}

	@Override
	public Player updatePlayer(Player player) {
		return playerDao.update(player);
	}

}
