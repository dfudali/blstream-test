package com.blstream.test.dao;

import java.util.List;

import com.blstream.test.model.Player;

public interface PlayerDAO {
	
	public Player add(Player player);
	
	public void deleteById(Long id);
	
	public Player get(Long id);
	
	public List<Player> getAll();
	
	public Player update(Player player);
	
}
