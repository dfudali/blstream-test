package com.blstream.test.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.blstream.test.model.Player;

@Repository
public class HibernatePlayerDAO implements PlayerDAO {

	private SessionFactory sessionFactory;

	@Override
	public Player add(Player player) {
		getCurrentSession().save(player);
		return player;
	}
	
	@Override
	public void deleteById(Long id) {
		Query query = getCurrentSession().createQuery("delete from Player p where p.id = :id");
		query.setParameter("id", id);
		query.executeUpdate();
	}
	
	@Override
	public Player get(Long id) {
		return (Player) getCurrentSession().get(Player.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Player> getAll() {
		Query query = getCurrentSession().createQuery("select p from Player p");
		return query.list();
	}

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public Player update(Player player) {
		Player old = get(player.getId());
		if(old != null) {
			player = (Player) getCurrentSession().merge(player);
		} else {
			player = null;
		}
		
		return player;
	}

}
