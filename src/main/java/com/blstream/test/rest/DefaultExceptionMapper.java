package com.blstream.test.rest;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class DefaultExceptionMapper implements ExceptionMapper<Exception> {

	@Override
	public Response toResponse(Exception exception) {
		Response response = null;

		Map<String, Object> responseBody = new HashMap<String, Object>();
		responseBody.put("error", "Error occured while accessing resource.");
		response = Response.status(Status.INTERNAL_SERVER_ERROR)
				.entity(responseBody).type(MediaType.APPLICATION_JSON).build();

		return response;
	}
}
