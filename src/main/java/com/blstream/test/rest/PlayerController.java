package com.blstream.test.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.blstream.test.model.Player;
import com.blstream.test.model.validation.Add;
import com.blstream.test.model.validation.Update;
import com.blstream.test.service.PlayerService;

@Component
@Path("/players")
public class PlayerController {

	private PlayerService playerService;

	private Validator validator;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response add(Player player) {
		Response response = null;

		Set<ConstraintViolation<Player>> errors = validator.validate(player,
				Add.class);
		if (errors.isEmpty()) {
			Player addedPlayer = playerService.addPlayer(player);
			response = Response.status(Status.CREATED).entity(addedPlayer)
					.build();
		} else {
			response = getResponseToValidationError(errors);
		}

		return response;
	}

	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") Long id) {
		Response response = null;

		playerService.deletePlayer(id);
		response = Response.ok().build();

		return response;
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@PathParam("id") Long id) {
		Response response = null;

		Player player = playerService.getPlayer(id);
		if (player == null) {
			response = Response.status(Status.NOT_FOUND).build();
		} else {
			response = Response.ok(player).build();
		}

		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll() {
		Response response = null;

		List<Player> players = playerService.getAllPlayers();
		response = Response.ok(players).build();

		return response;
	}

	private Map<String, List<String>> getConstrainViolationsAsMap(
			Set<ConstraintViolation<Player>> errors) {
		Map<String, List<String>> errorsByPropertyNames = new HashMap<String, List<String>>();
		for (ConstraintViolation<Player> error : errors) {
			String propertyName = error.getPropertyPath().toString();
			String message = error.getMessage();
			if (!errorsByPropertyNames.containsKey(propertyName)) {
				List<String> validationMessages = new ArrayList<String>();
				validationMessages.add(message);
				errorsByPropertyNames.put(propertyName, validationMessages);
			} else {
				errorsByPropertyNames.get(propertyName).add(message);
			}
		}
		return errorsByPropertyNames;
	}

	private Response getResponseToValidationError(
			Set<ConstraintViolation<Player>> errors) {
		Response response = null;
		Map<String, Object> responseBody = new HashMap<String, Object>();
		Map<String, List<String>> errorsByPropertyNames = getConstrainViolationsAsMap(errors);
		responseBody.put("error", "Error(s) occured while validating request.");
		responseBody.put("validation_errors", errorsByPropertyNames);

		response = Response.status(Status.BAD_REQUEST).entity(responseBody)
				.build();
		return response;
	}

	@Autowired
	public void setPlayerService(PlayerService playerService) {
		this.playerService = playerService;
	}

	@Autowired
	public void setValidator(Validator validator) {
		this.validator = validator;
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response update(Player player) {
		Response response = null;

		Set<ConstraintViolation<Player>> errors = validator.validate(player,
				Update.class);
		if (errors.isEmpty()) {
			Player updated = playerService.updatePlayer(player);
			if (updated == null) {
				response = Response.status(Status.NOT_FOUND).build();
			} else {
				response = Response.ok().build();
			}
		} else {
			response = getResponseToValidationError(errors);
		}

		return response;
	}

}
