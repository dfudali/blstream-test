drop table if exists players;

create table players (
	player_id int not null IDENTITY,
	name varchar(50) not null,
	version int default 0,
	unique(name)
);